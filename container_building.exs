alias Buildah.{Cmd, Dnf, Print}

catatonit_cmd = "/usr/libexec/catatonit/catatonit"

dnf_catatonit_on_container = fn (container, options) ->
    Dnf.packages_no_cache(container, [
        "catatonit"
    ], options)
    Cmd.run(
        container,
        ["printenv", "PATH"],
        into: IO.stream(:stdio, :line)
    )
    Cmd.run(
        container,
        ["rpm", "--query", "--list", "--verbose", "catatonit"],
        into: IO.stream(:stdio, :line)
    )
    Cmd.run(
        container,
        ["/bin/sh", "-c", "command -v " <> catatonit_cmd],
        into: IO.stream(:stdio, :line)
    )
    {catatonit_exec_, 0} = Cmd.run(
        container,
        ["/bin/sh", "-c", "command -v " <> catatonit_cmd]
    )
    {_, 0} = Cmd.config(
        container,
        entrypoint: "[\"#{String.trim(catatonit_exec_)}\", \"--\"]"
    )
end

dnf_catatonit_test = fn (container, image_ID, options) ->
    {_, 0} = Cmd.run(container, [catatonit_cmd, "--version"], into: IO.stream(:stdio, :line))
    # {_, 0} = Podman.Cmd.run(image_ID, [catatonit_cmd, "--version"],
    #     tty: true, rm: true, into: IO.stream(:stdio, :line)
    # )
    # Error: cannot open sd-bus: No such file or directory:
    # OCI runtime command not found error
    Dnf.packages_no_cache(container, ["psmisc"], options)
    {_, 0} = Cmd.run(container, ["pstree"], into: IO.stream(:stdio, :line))
    # {_, 0} = Podman.Cmd.run(image_ID, ["pstree"],
    #     tty: true, rm: true, into: IO.stream(:stdio, :line)
    # )
end




{whoami_, 0} = System.cmd("whoami", [])
"root" = String.trim(whoami_)
{_, 0} = Cmd.version(into: IO.stream(:stdio, :line))
{_, 0} = Cmd.info(into: IO.stream(:stdio, :line))

# build_image = "fedora"
# _image = "localhost/" <> build_image

# from_image = "docker.io/" <> build_image
from_image = System.fetch_env!("FROM_IMAGE")
IO.puts(from_image)

# image = "localhost/" <> build_image
# image = System.fetch_env!("IMAGE")
# IO.puts(image)

# destination = "docker://#{System.fetch_env!("CI_REGISTRY_IMAGE")}/" <> build_image
destination = System.fetch_env!("IMAGE_DESTINATION")
IO.puts(destination)

Buildah.from_push(
    from_image,
    dnf_catatonit_on_container,
    dnf_catatonit_test,
    destination: destination,
    quiet: System.get_env("QUIET")
)

Print.images()
